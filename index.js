const fs = require('fs')
const util = require('util')
const crypto = require('crypto')

const generateKeyPair = util.promisify(crypto.generateKeyPair)
const writeFile = util.promisify(fs.writeFile)
const mkdir = util.promisify(fs.mkdir)

const date = () => (new Date()).toJSON()

;(async () => {
  process.stdout.write(`${date()} Generating new key pair\n`)

  const { privateKey, publicKey } = await generateKeyPair('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: { type: 'spki', format: 'pem' },
    privateKeyEncoding: { type: 'pkcs8', format: 'pem', cipher: 'aes-256-cbc', passphrase: '' }
  })

  await mkdir('./keys', { recursive: true })
  await writeFile('./keys/rsa.key', privateKey)
  await writeFile('./keys/rsa.key.pub', publicKey)

  process.stdout.write(`${date()} Done\n`)
})().catch((error) => process.stderr.write(`${error}\n`))
