resource "kubernetes_persistent_volume_claim" "charon" {
  metadata {
    name = "charon"
  }
  spec {
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = "do-block-storage"
    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}

resource "kubernetes_cron_job" "charon" {
  depends_on = [kubernetes_persistent_volume_claim.charon]
  metadata {
    name = "charon"
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 5
    schedule                      = "0 0 * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10
    job_template {
      metadata {}
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 10
        template {
          metadata {}
          spec {
            container {
              name  = "charon"
              image = "registry.digitalocean.com/c3po/charon:0.0.1"
              volume_mount {
                mount_path = "/usr/src/charon/keys"
                name       = "charon"
              }
            }
            volume {
              name = "charon"
              persistent_volume_claim {
                claim_name = "charon"
              }
            }
          }
        }
      }
    }
  }
}
