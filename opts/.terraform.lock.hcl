# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.4.0"
  constraints = "2.4.0"
  hashes = [
    "h1:fkV6fx8AMyTQGdp8A6Z4idlsCSKtmoZ6+zXSksFDKaw=",
    "zh:20112c283be23e1cc05af7dbf9bb69df851dd682658a54aeff58612f7663735f",
    "zh:40899dd3fc4ad8df745ecd95d35d4ef47977d7a797141f1ffcdb9ad2d31759b8",
    "zh:55d0922f822a22d0dad2cebc2e9c51d6980781f54a5c73d230d3a7fdbede6dab",
    "zh:75b6010da598a5272fec6acaf6f72f0cce0f669808170fcf1c14f97a6efa1505",
    "zh:97cabbe68c89c06e23b4affea55f5c2397a7ab04d7ec34d49c7b7b2d00e58f12",
    "zh:adbf9b02aa9101c5f22e3b582bad41c9087af4cd52740c04c5ebb9840f1fc1c6",
    "zh:ba43c607f6871561da4a79893c31c0f63c808351cdae6ff2e39c0cf8d8355632",
    "zh:bd29fd8225c90d09d220e94a447937e38377b92669d077baa4bb746e7b94af27",
    "zh:c82f3900f9e67b79701ec622d15c753b33540ad8d52d34a1f6ab437f24f014af",
    "zh:d0a80faf997544cb8ae4057f7900d99703165fb86dc37f8204bf2935f4dc4ea7",
    "zh:d7ca52e5523a1ce10903cb493739fd88f5a2648145fad08531e92ab3830d7dea",
    "zh:ec43ddb44c951769252ddb31e38a02c4be3d66bc1a43ebd786a24e96be8e01fa",
    "zh:fd9d7b326b3b7bd819cfa7c1951ad12e96a811972fad36007154d1b4555f5d0d",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.0.2"
  hashes = [
    "h1:PRfDnUFBD4ud7SgsMAa5S2Gd60FeriD1PWE6EifjXB0=",
    "zh:4e66d509c828b0a2e599a567ad470bf85ebada62788aead87a8fb621301dec55",
    "zh:55ca6466a82f60d2c9798d171edafacc9ea4991aa7aa32ed5d82d6831cf44542",
    "zh:65741e6910c8b1322d9aef5dda4d98d1e6409aebc5514b518f46019cd06e1b47",
    "zh:79456ca037c19983977285703f19f4b04f7eadcf8eb6af21f5ea615026271578",
    "zh:7c39ced4dc44181296721715005e390021770077012c206ab4c209fb704b34d0",
    "zh:86856c82a6444c19b3e3005e91408ac68eb010c9218c4c4119fc59300b107026",
    "zh:999865090c72fa9b85c45e76b20839da51714ae429d1ab14b7d8ce66c2655abf",
    "zh:a3ea0ae37c61b4bfe81f7a395fb7b5ba61564e7d716d7a191372c3c983271d13",
    "zh:d9061861822933ebb2765fa691aeed2930ee495bfb6f72a5bdd88f43ccd9e038",
    "zh:e04adbe0d5597d1fdd4f418be19c9df171f1d709009f63b8ce1239b71b4fa45a",
  ]
}
