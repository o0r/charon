terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.4.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
}

provider "digitalocean" {
  token = var.digitalocean_token
}

data "digitalocean_kubernetes_cluster" "c3po" {
  name = "c3po"
}

provider "kubernetes" {
  host  = data.digitalocean_kubernetes_cluster.c3po.endpoint
  token = data.digitalocean_kubernetes_cluster.c3po.kube_config[0].token
  cluster_ca_certificate = base64decode(
    data.digitalocean_kubernetes_cluster.c3po.kube_config[0].cluster_ca_certificate
  )
}

