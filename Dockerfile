FROM node:15-alpine

VOLUME ["/usr/src/charon/keys"]

WORKDIR /usr/src/charon

COPY package*.json ./

RUN npm install --only production

COPY . .

CMD ["npm", "start"]

